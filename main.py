import random
import time
import cv2
import mediapipe#pip install mediapipe 
from PyQt5 import QtWidgets, uic
import sys
import datetime
import threading#pip install threading 

log  = 'История\n'
time_bud = '100'
minm = []
comand_txt=['РАЗ','Как Дела','Привет']
camera = cv2.VideoCapture(0)
drawingModule = mediapipe.solutions.drawing_utils
handsModule = mediapipe.solutions.hands
frameWidth = camera.get(cv2.CAP_PROP_FRAME_WIDTH)
frameHeight = camera.get(cv2.CAP_PROP_FRAME_HEIGHT)
mp_drawing = mediapipe.solutions.drawing_utils
mp_holistic = mediapipe.solutions.holistic


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        import os
        filename = "II_CAM_Setting.ui"
        if '_MEIPASS2' in os.environ:
            filename = os.path.join(os.environ['_MEIPASS2'], filename)
        self.iu = uic.loadUi(filename, self)
        self.iu.pushButton.clicked.connect(self.returnPressedSlot)

        self.iu.pushButton_2.clicked.connect(self.closes)
    def returnPressedSlot(self):
        global time_bud

        time_bud = str(self.iu.timeEdit.text())



        self.iu.textBrowser.setText(log+f'Будильник стоит на время {self.iu.timeEdit.text()}')
        minm.clear()
        minm.append(self.iu.comboBox.currentText())
        minm.append(self.iu.comboBox_2.currentText())
        minm.append(self.iu.comboBox_3.currentText())


    def closes(self):
        sys.exit()






def person_all_cap(frame):
    with mp_holistic.Holistic(
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as holistic:

            image = cv2.cvtColor(cv2.flip(frame, 1), cv2.COLOR_BGR2RGB)
            image.flags.writeable = False
            results = holistic.process(image)

            image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    return image


def hand_mim(frame):
    with handsModule.Hands(static_image_mode=False, min_detection_confidence=0.7, min_tracking_confidence=0.7,
                           max_num_hands=2) as hands:
        results = hands.process(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        start_xy:int
        fingers = {4:None,8:None,5:None,12:None,9:None,16:None,13:None,20:None,17:None}
        if results.multi_hand_landmarks != None:
            for handLandmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(frame,handLandmarks,mp_holistic.HAND_CONNECTIONS)
                for id, point in enumerate(handLandmarks.landmark):
                    if id in fingers.keys():
                        fingers[id] = start_xy - point.y
                    elif id == 0:
                        start_xy = point.y

            if fingers[8] < fingers[5] and fingers[12] < fingers[9] and fingers[16] < fingers[13] and fingers[20] < \
                    fingers[17]:
                return 'Кулак✊'

            elif fingers[8]>fingers[5] and fingers[12]>fingers[9] and fingers[16]<fingers[13] and fingers[20]<fingers[17]:
                return 'Ножницы✌'

            elif fingers[8] - fingers[4] < 0.04:
                return 'ОК👌'

            elif fingers[8]>fingers[5] and fingers[12]>fingers[9] and fingers[16]>fingers[13] and fingers[20]>fingers[17]:
                return 'Ладонь🖐️'

        return ''


def body_mim(frame):
    with mp_holistic.Holistic(
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as holistic:
        image = cv2.cvtColor(cv2.flip(frame, 1), cv2.COLOR_BGR2RGB)
        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        results = holistic.process(image)
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        if results.pose_landmarks!=None:
            mp_drawing.draw_landmarks(
                image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS)
            landmarks = results.pose_landmarks.landmark

            if landmarks[1].x < landmarks[1].y:
                return  'Лежит'
            else:
                return 'Встал'

        return ''




def main():
    day = -1
    old_time = -1
    run = False
    while camera.isOpened():

        if time_bud != '100':

            now = datetime.timedelta(hours=datetime.datetime.now().hour,minutes=datetime.datetime.now().minute)
            time_budil = datetime.timedelta(hours=int(time_bud.split(':')[0]), minutes=int(time_bud.split(':')[1]))

            str_time_now = str(now).split()[-1].split(':')
            str_time_pass = str(time_budil).split()[-1].split(':')
            if str_time_now[0] == str_time_pass[0] and str_time_now[1] == str_time_pass[1] and (now.days != day or old_time!=now):
                print('Рота Подем')
                run = True
                _, frame = camera.read()
                info_body = body_mim(frame)
                cv2.imshow('test', person_all_cap(frame))
                if info_body == 'Встал':

                    print('Человек встал')
                    cv2.destroyAllWindows()
                    day = now.days
                    old_time = now
                    run = False

            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        if not run:
            _, frame = camera.read()
            info_hand = hand_mim(frame)
            cv2.imshow('test', frame)
            if info_hand!='':
                if info_hand in minm:
                    do = comand_txt[minm.index(info_hand)]
                    if do == 'РАЗ':
                        print('Камень ножници бумага')
                        time.sleep(0.5)
                        print('Раз')
                        time.sleep(0.5)
                        print('Два')
                        time.sleep(0.95)
                        print('ТРИ')
                        time.sleep(0.75)
                        print(random.choice(['Ты выиграл','Ты проиграл']))
                    print(do)


if __name__ == '__main__':
    t = threading.Thread(target=main, args=())
    t.start()
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()



    cv2.destroyAllWindows()
    sys.exit(app.exec())
